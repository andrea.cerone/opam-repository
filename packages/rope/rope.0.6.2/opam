opam-version: "2.0"
maintainer: "Christophe Troestler <Christophe.Troestler@umons.ac.be>"
authors: [ "Christophe Troestler" ]
license: "LGPL-2.1-only WITH OCaml-LGPL-linking-exception"
homepage: "https://github.com/Chris00/ocaml-rope"
dev-repo: "git+https://github.com/Chris00/ocaml-rope.git"
bug-reports: "https://github.com/Chris00/ocaml-rope/issues"
doc: "https://Chris00.github.io/ocaml-rope/doc"
tags: [ "datastructure" ]
build: [
  ["dune" "subst"] {dev}
  ["dune" "build" "-p" name "-j" jobs]
  ["dune" "runtest" "-p" name "-j" jobs] {with-test}
]
depends: [
  "ocaml" {>= "4.03.0" & < "5.0.0"}
  "base-bytes"
  "dune"
  "benchmark" {with-test}
]
synopsis: "Ropes (heavyweight strings)"
description: """
Ropes ("heavyweight strings") are a scalable string implementation:
they are designed for efficient operation that involve the string as a
whole.  Operations such as concatenation, and substring take time that
is nearly independent of the length of the string.  Unlike strings,
ropes are a reasonable representation for very long strings such as
edit buffers or mail messages.
"""
url {
  src:
    "https://github.com/Chris00/ocaml-rope/releases/download/0.6.2/rope-0.6.2.tbz"
  checksum: [
    "md5=b1bd36276f4556af2b208b70b92011d5"
    "sha256=514001a5f6a972205cc102924f1c863ad268ff74a31dd0e43b5deea081729b95"
    "sha512=cc4d7cb708e579c0f23e036680e42b45886d13b1bcf3323281060c5aaf1bee8a804f8f53762c57f4df557a77a6c85a24c8fd588f76ce70c9147236e4b8733b3d"
  ]
}
