opam-version: "2.0"
maintainer: "Jane Street developers"
authors: ["Jane Street Group, LLC"]
homepage: "https://github.com/janestreet/jst-config"
bug-reports: "https://github.com/janestreet/jst-config/issues"
dev-repo: "git+https://github.com/janestreet/jst-config.git"
doc: "https://ocaml.janestreet.com/ocaml-core/latest/doc/jst-config/index.html"
license: "MIT"
build: [
  ["dune" "build" "-p" name "-j" jobs]
]
depends: [
  "ocaml"             {>= "4.08.0"}
  "base"              {>= "v0.15" & < "v0.16"}
  "ppx_assert"        {>= "v0.15" & < "v0.16"}
  "dune"              {>= "2.0.0"}
  "dune-configurator"
]
synopsis: "Compile-time configuration for Jane Street libraries"
description: "
Defines compile-time constants used in Jane Street libraries such as Base, Core, and
Async.

This package has an unstable interface; it is intended only to share configuration between
different packages from Jane Street. Future updates may not be backward-compatible, and we
do not recommend using this package directly.
"
url {
  src:
    "https://github.com/janestreet/jst-config/archive/refs/tags/v0.15.1.tar.gz"
  checksum: [
    "sha256=7053e5bbb3016b760c1e8f419814ad059625105db25dafa020676fcfc0f3b41b"
    "sha512=389854aad6775756fd51374a036c748947f186eae4b48a4863549e7d8efafee25bdfc7c525dd2c466d9f0e0265b48a6e7b950042b6c9014051f8288393173147"
  ]
}
